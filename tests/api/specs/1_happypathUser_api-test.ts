import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";
import { RegisterController } from "../lib/controllers/register.controller";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";


const registerController = new RegisterController();
const usersController = new UsersController();
const authController = new AuthController();

let userDataToUpdate;
let userTotest = global.appConfig.userTotest.User;

describe(`Happy path tests user controller`, () => {
    let accessToken: string;
    let realId: number;

    before(`Should register new user`, async () => {
        let response = await registerController.register(userTotest);
        checkStatusCode(response, 201);
        checkResponseTime(response,1000);
    });

    it(`Should get all users`, async () => {
        let response = await usersController.getAllUsers();

        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
    });

    it(`Should login and get the token`, async () => {
        let response = await authController.login(userTotest.email, userTotest.password);
        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
        accessToken = response.body.token.accessToken.token;
    });

    it(`Should return current user by token`, async () => {
        let response = await usersController.getCurrentUser(accessToken);
        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
        realId = Number(response.body.id);
    });

    it(`Should update username using valid data`, async () => {
        userDataToUpdate = {
            id: realId,
            avatar: userTotest.avatar,
            email: userTotest.email,
            userName: "NewUsernameApiTest",
        };

        let response = await usersController.updateUser(userDataToUpdate, accessToken);
        checkStatusCode(response, 204);
        checkResponseTime(response,1000);
    });

    it(`Should return current user by token`, async () => {
        let response = await usersController.getCurrentUser(accessToken);
        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
    });

    it(`Should return user details when getting user details with valid id`, async () => {
        let response = await usersController.getUserById(realId);
        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
    });

    after('Should delete a newly created user', async () => {
        let response = await usersController.deleteUserById(realId, accessToken);
        checkStatusCode(response, 204);
        checkResponseTime(response,1000);
    });
});
