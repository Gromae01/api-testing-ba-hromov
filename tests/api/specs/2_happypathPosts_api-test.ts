import { expect } from "chai";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";
import { RegisterController } from "../lib/controllers/register.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { CommentsController } from "../lib/controllers/comments.controller";
import { PostsController } from "../lib/controllers/posts.controller";
import { UsersController } from "../lib/controllers/users.controller";


const registerController = new RegisterController();
const postsController = new PostsController();
const authController = new AuthController();
const userController = new UsersController();
const commentsController = new CommentsController();

let userTotest = global.appConfig.userTotest.User;

describe(`Happy path tests posts controller`, () => {
    let accessToken: string;
    let realId: number;

    before(`Should register new user, login to get the token and get user real id by getting user by token`, async () => {
        let response1 = await registerController.register(userTotest);
        checkStatusCode(response1, 201);
        checkResponseTime(response1,1000);

        let response2 = await authController.login(userTotest.email, userTotest.password);
        checkStatusCode(response2, 200);
        checkResponseTime(response2,1000);
        accessToken = response2.body.token.accessToken.token;

        let response3 = await userController.getCurrentUser(accessToken);
        checkStatusCode(response3, 200);
        checkResponseTime(response3,1000);
        realId = Number(response3.body.id);
    });

    it(`Should create new post using valid data`, async () => {
        let postDataToCreate = {
            authorId: realId,
            previewImage: "image",
            body: "NewPostBody"
        };

        let response = await postsController.createNewPost(postDataToCreate, accessToken);
        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
    });

    it(`Should add like to existing post`, async () => {
        let likeDataToCreate = {
            entityId: 1244,
            isLike: true,
            userId: realId
        };

        let response = await postsController.likePost(likeDataToCreate, accessToken);
        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
    });

    it(`Should create new comment using valid data`, async () => {
        let commentDataToCreate = {
            authorId: realId,
            postId: 1244,
            body: "NewCommentBody"
        };

        let response = await commentsController.createNewComment(commentDataToCreate, accessToken);
        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
    });

    it(`Should get all existing posts`, async () => {
        let response = await postsController.getAllPosts();
        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
        expect(response.body.length, `Response body should have more than 1 item`).to.be.greaterThan(1); 

    });

    after('Should delete a newly created user', async () => {
        let response = await userController.deleteUserById(realId, accessToken);
        checkStatusCode(response, 204);
        checkResponseTime(response,1000);
    });

});
