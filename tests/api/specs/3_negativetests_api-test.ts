import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";
import { RegisterController } from "../lib/controllers/register.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { CommentsController } from "../lib/controllers/comments.controller";
import { PostsController } from "../lib/controllers/posts.controller";
import { UsersController } from "../lib/controllers/users.controller";

const registerController = new RegisterController();
const postsController = new PostsController();
const authController = new AuthController();
const usersController = new UsersController();
const commentsController = new CommentsController();

let userTotest = global.appConfig.userTotest.User;

describe(`Happy path tests posts controller`, () => {
    let accessToken: string;
    let realId: number;

    before(`Should register new user, login to get the token and get user real id by getting user by token`, async () => {
        let response1 = await registerController.register(userTotest);
        checkStatusCode(response1, 201);
        checkResponseTime(response1,1000);

        let response2 = await authController.login(userTotest.email, userTotest.password);
        checkStatusCode(response2, 200);
        checkResponseTime(response2,1000);
        accessToken = response2.body.token.accessToken.token;

        let response3 = await usersController.getCurrentUser(accessToken);
        checkStatusCode(response3, 200);
        checkResponseTime(response3,1000);
        realId = Number(response3.body.id);
    });



    it(`Shouldn't update username, because of invalid data`, async () => {
        let userDataToUpdate = {
            id: 0,
            avatar: userTotest.avatar,
            email: userTotest.email,
            userName: "NewUsernameApiTest",
        };

        let response = await usersController.updateUser(userDataToUpdate, accessToken);
        checkStatusCode(response, 404);
        checkResponseTime(response,1000);
    });

    it(`Shouldn't create new post, due to invalid token`, async () => {
        let postDataToCreate = {
            authorId: 0,
            previewImage: "image",
            body: "NewPostBody"
        };

        let response = await postsController.createNewPost(postDataToCreate, "badtoken");
        checkStatusCode(response, 401);
        checkResponseTime(response,1000);
    });

    it(`Shouldn't create new comment, due to invalid data`, async () => {
        let commentDataToCreate = {
            authorId: 0,
            postId: 0,
            body: "NewCommentBody"
        };

        let response = await commentsController.createNewComment(commentDataToCreate, accessToken);
        checkStatusCode(response, 500);
        checkResponseTime(response,1000);
    });

    after('Should delete a newly created user', async () => {
        let response = await usersController.deleteUserById(realId, accessToken);
        checkStatusCode(response, 204);
        checkResponseTime(response,1000);
    });
});
